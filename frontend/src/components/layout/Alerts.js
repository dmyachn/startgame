import React, { Component, Fragment } from 'react';
import { withAlert } from 'react-alert';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

export class Alerts extends Component {
  static propTypes = {
    error: PropTypes.object.isRequired,
    message: PropTypes.object.isRequired,
  };

  componentDidUpdate(prevProps) {
    const { error, alert, message } = this.props;

    if (error !== prevProps.error) {
      if (error.msg.news_title) alert.error(`Заголовок: ${error.msg.news_title.join()}`);
      if (error.msg.news_text) alert.error(`Текст новости: ${error.msg.news_title.join()}`);
      if (error.msg.non_field_errors) alert.error(error.msg.non_field_errors.join());
      if (error.msg.username) alert.error(error.msg.username.join());
    }

    if (message !== prevProps.message) {
      if (message.deleteNews) alert.success(message.deleteNews);
      if (message.addNews) alert.success(message.addNews);
      if (message.deleteThem) alert.success(message.deleteThem);
      if (message.addThem) alert.success(message.addThem);
      if (message.deleteLike) alert.success(message.deleteLike);
      if (message.addLike) alert.success(message.addLike);
      if (message.editNews) alert.success(message.editNews);
      if (message.editThem) alert.success(message.editThem);
      if (message.noAuthenticated) alert.error(message.noAuthenticated);
      if (message.passwordNotMatch) alert.error(message.passwordNotMatch);
    }
  }

  render() {
    return <Fragment />;
  }
}
 
const mapStateToProps = (state) => ({
  error: state.errors,
  message: state.messages,
});

export default connect(mapStateToProps)(withAlert()(Alerts));