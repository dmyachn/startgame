import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { logout } from '../../actions/auth';

export class Header extends Component {

    static propTypes = {
        auth: PropTypes.object.isRequired,
        logout: PropTypes.func.isRequired,
      };

    render() {
        const { isAuthenticated, user } = this.props.auth;

        const authLinks = (
          <ul className="navbar-nav ml-auto mt-2 mt-lg-0">
            <span className="navbar-text mr-3">
              <strong>{user ? `Добро пожаловать ${user.username}` : ''}</strong>
            </span>
            <li className="nav-item">
              <Link to="/liked">
              <button onClick={this.props.logout} className="btn btn-info btn-sm">
                        Выйти
              </button>
              </Link>
            </li>
          </ul>
        );
    
        const guestLinks = (
          <ul className="navbar-nav ml-auto mt-2 mt-lg-0">
            <li className="nav-item">
              <Link to="/register" className="nav-link">
                Зарегистрироваться
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/login" className="nav-link">
                Войти
              </Link>
            </li>
          </ul>
        );

        return (
          <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <a className="navbar-brand" href="#">StartGame</a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
          {isAuthenticated &&
            <ul className="navbar-nav">
              <li className="nav-item">
              <Link to="/liked" className="nav-link">
                        Избранное
                      </Link>
              </li>
              {user.is_superuser &&
              <li className="nav-item">
              <Link to="/thems" className="nav-link">
                        Управление тематиками
                      </Link>
              </li>
              }
            </ul>
            }
          {isAuthenticated ? authLinks : guestLinks}
          </div>
        </nav>
        )
    }
}


const mapStateToProps = (state) => ({
    auth: state.auth,
  });
  
  export default connect(mapStateToProps, { logout })(Header);
