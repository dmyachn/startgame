import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import {getThems, editThem, addThem} from "../../actions/thems"


export class EditThem extends Component {
    state={
      id: this.props.id,
      them_title: this.props.them_title,
    }

    static propTypes = {
      editThem: PropTypes.func.isRequired,
        auth: PropTypes.object.isRequired,

    }

    onChange = e => this.setState({[e.target.name]: e.target.value});

    onSubmit = e => {
        e.preventDefault();
        const{id, them_title} = this.state;
        const them = {id, them_title};
        this.props.editThem(this.state.id,them);
    };

    render() {
        const{them_title} = this.state;
        return (
         
            <Fragment>
            
      <div className="card card-body mt-4 mb-4">
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Название темы</label>
            <input
              className="form-control"
              type="text"
              name="them_title"
              onChange={this.onChange}
              value={them_title}
            />
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-primary" >
              Изменить
            </button>
          </div>
        </form>
      </div>
      </Fragment>
    );
    }
}
const mapStatetoProps = state => ({
  thems: state.thems.thems,
  auth: state.auth,
})
export default connect(mapStatetoProps, { editThem })(EditThem);
