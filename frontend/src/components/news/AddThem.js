import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import {getThems, addThem} from "../../actions/thems"


export class AddThem extends Component {
    state={
      them_title: "",
    }

    static propTypes = {
      addThem: PropTypes.func.isRequired,
        auth: PropTypes.object.isRequired,

    }

    onChange = e => this.setState({[e.target.name]: e.target.value});

    onSubmit = e => {
        e.preventDefault();
        const{them_title} = this.state;
        const them = {them_title};
        this.props.addThem(them);
        this.handleClick()
    };

    handleClick = () => {
      this.props.toggle();
     };

    render() {
        const{them_title} = this.state;
        return (
          <div className="popUpContainer">
          <span className="close" onClick={this.handleClick}>&times;    </span>
            <Fragment>
            
      <div className="card card-body mt-4 mb-4">
        <h2>Добавить тему</h2>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Название темы</label>
            <input
              className="form-control"
              type="text"
              name="them_title"
              onChange={this.onChange}
              value={them_title}
            />
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-primary" >
              Добавить
            </button>
          </div>
        </form>
      </div>
      </Fragment>
      </div>
    );
    }
}
const mapStatetoProps = state => ({
  thems: state.thems.thems,
  auth: state.auth,
})
export default connect(mapStatetoProps, { addThem })(AddThem);
