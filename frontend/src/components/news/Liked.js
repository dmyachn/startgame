import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes, { array } from 'prop-types';
import { getNews, deleteNews } from '../../actions/news'
import { addLike, deleteLike } from '../../actions/likes'
import news from '../../reducers/news';
import like from '../../reducers/like';
import { createMessage } from '../../actions/messages';
import Form from './Form';
import { Link, Redirect } from 'react-router-dom';
import {getThems} from "../../actions/thems"
import EditForm from './EditForm';




export class Liked extends Component {

    static propTypes = {
        news: PropTypes.array.isRequired,
        getNews: PropTypes.func.isRequired,
        deleteNews: PropTypes.func.isRequired,
        addLike: PropTypes.func.isRequired,
        deleteLike: PropTypes.func.isRequired
    };

    state = {
        seenAdd: false,
        seenEdit: false
        };

        togglePopAdd = () => {
            this.setState({
                seenAdd: !this.state.seenAdd
            });
           };
        
           togglePopEdit = () => {
            this.setState({
                seenEdit: !this.state.seenEdit
            });
           };
           
    componentDidMount() {
        this.props.getNews();
        this.props.getThems();
    }


    message(){
        this.props.createMessage({ noAuthenticated: 'Для того чтобы добавить новость в избранное необходимо войти' })
    }

    likeTongle(isLiked, userl, newsl){
        if (isLiked){
            newsl.likes.map(like => {
                if (like.user==userl.id){           
                    this.props.deleteLike(like)
                    return;
                }
            })
        }
        else{
            const user=userl.id;
            const news=newsl.id;
            const like = {user, news}
            this.props.addLike(like)
        }
    }

    isUserInLikes(user, news){
        if(!user) return false;
        var result=false
        news.likes.map(like => {
            if (like.user==user.id){
                result=true;
                return;
            }
        });
        return result;
    }

    addNewTongle(map,newsId,value){
        map.set(newsId, value);
        return value;
    }

    hasAcces(isAuthenticated,user,news){
        if(isAuthenticated)
            return user.is_superuser||user.id==news.news_author;
        return false;
    }
    getLocalTime(pub_date){
        var date = new Date(pub_date);
        var options = {year: 'numeric', month: 'numeric', day: 'numeric', hour:'numeric', minute:'numeric', second:'numeric'};
        options.timeZone = 'Europe/Moscow';
        return date.toLocaleDateString('ru-RU', options);
    }
    getThemName(news){
        var result="";
        this.props.thems.map(them =>{
            if (them.id==news.them)
                result=them.them_title
        })
        return result;
    }
    render() {
        if (!this.props.auth.isAuthenticated) {
          return <Redirect to="/" />;
        }

        const { isAuthenticated, user } = this.props.auth;
        const redHeart = (<nobr>❤</nobr>)
        const whiteHeart = (<nobr>🤍</nobr>)
        var tongle = new Map()
        return (
            <Fragment>
                <h2>Избранное</h2>
                <table className="table table-borderless">
                    <tbody>
                        {this.props.news.slice(0).reverse().map(news => (this.isUserInLikes(user,news) &&
                            <tr key={news.id}>
                                <td>
                                {this.hasAcces(isAuthenticated,user,news) &&
                                            <div style={{ display: "flex" }}>
                                                <div  onClick={this.togglePopEdit}>
                                                <button className="btn btn-warning btn-sm">Изменить</button>
                                                </div>
                                                <button onClick={this.props.deleteNews.bind(this, news.id)}
                                                className="btn btn-danger btn-sm"
                                                style={{ marginLeft: "auto"}}>
                                                    Удалить
                                                </button>
                                            </div>}
                                    <div className="card">
                                        <div className="card-body">
                                            <h6 className="card-subtitle mb-2 text-muted">{this.getThemName(news)}</h6>
                                            <h5 className="card-title">{news.news_title}</h5>
                                            <h6 className="card-subtitle mb-2 text-muted"><small>{this.getLocalTime(news.pub_date)} </small></h6>
                                            <p className="card-text" style={{whiteSpace: "pre-line"}}>{news.news_text}</p>
                                            <button onClick={isAuthenticated ? this.likeTongle.bind(this,this.isUserInLikes(user,news),user,news) : this.message.bind(this)}
                                                className="btn btn-light btn-sm"
                                                style={{ marginLeft: "auto"}}>
                                                {this.addNewTongle(tongle,news.id, this.isUserInLikes(user,news))}
                                                 {isAuthenticated ? this.isUserInLikes(user,news) ? redHeart : whiteHeart : whiteHeart} {news.likes.length}
                                            </button>
                                            

                                            {this.state.seenEdit ? <EditForm toggle={this.togglePopEdit} id={news.id} them={news.them} news_text={news.news_text} news_title={news.news_title} news_author={news.news_author} pub_date={news.pub_date} /> : null}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </Fragment>
        )
    }
}

const mapStatetoProps = state => ({
    thems: state.thems.thems,
    news: state.news.news,
    auth: state.auth,
})

export default connect(mapStatetoProps, { getNews, deleteNews, createMessage, addLike, deleteLike, getThems })(Liked)