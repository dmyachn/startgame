import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes, { array } from 'prop-types';
import { getThems, deleteThem } from '../../actions/thems'
import news from '../../reducers/news';
import like from '../../reducers/like';
import { createMessage } from '../../actions/messages';
import AddThem from './AddThem';
import { Link, Redirect } from 'react-router-dom';
import EditThem from './EditThem';




export class Them extends Component {

    static propTypes = {
        thems: PropTypes.array.isRequired,
        getThems: PropTypes.func.isRequired,
        deleteThem: PropTypes.func.isRequired,
    };

    state = {
        seenAdd: false,
        seenEdit: false
        };

        togglePopAdd = () => {
            this.setState({
                seenAdd: !this.state.seenAdd
            });
           };
        
           togglePopEdit = () => {
            this.setState({
                seenEdit: !this.state.seenEdit
            });
           };
           
    componentDidMount() {
        this.props.getThems();
    }
    
    render() {
        if (!this.props.auth.isAuthenticated) {
          return <Redirect to="/" />;
        }
        return (
            <Fragment>
                <h2>Редактирование тематик</h2>
                <div className="container">
                        <div  onClick={this.togglePopAdd}>
                        <button className="btn btn-info btn-sm">Добавить тему</button>
                        </div>
                        {this.state.seenAdd ? <AddThem toggle={this.togglePopAdd} /> : null}
                    </div>
                
                <table className="table table-borderless">
                    <tbody>
                        {this.props.thems.map(them => (
                            <tr key={them.id}>
                                <td>
                                            <div style={{ display: "flex" }}>
                                                <button onClick={this.props.deleteThem.bind(this, them.id)}
                                                className="btn btn-danger btn-sm"
                                                style={{ marginLeft: "auto"}}>
                                                    Удалить
                                                </button>
                                            </div>
                                    <div className="card">
                                        <div className="card-body">
                                        <EditThem id={them.id} them_title={them.them_title}/>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </Fragment>
        )
    }
}

const mapStatetoProps = state => ({
    thems: state.thems.thems,
    auth: state.auth,
})

export default connect(mapStatetoProps, { getThems, deleteThem, createMessage})(Them)