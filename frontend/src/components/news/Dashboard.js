import React, { Fragment } from 'react';
import News from './News';

export default function Dashboard() {
    return (
        <Fragment>
            <News />
        </Fragment>
    );
}
