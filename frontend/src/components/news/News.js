import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes, { array } from 'prop-types';
import {getThems} from "../../actions/thems"
import { getNews, deleteNews } from '../../actions/news'
import { addLike, deleteLike } from '../../actions/likes'
import news from '../../reducers/news';
import like from '../../reducers/like';
import { createMessage } from '../../actions/messages';
import Form from './Form';
import EditForm from './EditForm';




export class News extends Component {

    static propTypes = {
        news: PropTypes.array.isRequired,
        getNews: PropTypes.func.isRequired,
        deleteNews: PropTypes.func.isRequired,
        addLike: PropTypes.func.isRequired,
        deleteLike: PropTypes.func.isRequired
    };

    state = {
        them: 0,
        seenAdd: false,
        seenEdit: false
        };

        togglePopAdd = () => {
            this.setState({
                seenAdd: !this.state.seenAdd
            });
           };
        
           togglePopEdit = () => {
            this.setState({
                seenEdit: !this.state.seenEdit
            });
           };
           
    componentDidMount() {
        this.props.getThems();
        this.props.getNews();
    }

    onChange = e => this.setState({[e.target.name]: e.target.value});
    message(){
        this.props.createMessage({ noAuthenticated: 'Для того чтобы добавить новость в избранное необходимо войти' })
    }

    likeTongle(isLiked, userl, newsl){
        if (isLiked){
            newsl.likes.map(like => {
                if (like.user==userl.id){            
                    this.props.deleteLike(like)
                    return;
                }
            })
        }
        else{
            const user=userl.id;
            const news=newsl.id;
            const like = {user, news}
            this.props.addLike(like)
        }
    }

    isUserInLikes(user, news){
        if(!user) return false;
        var result=false
        news.likes.map(like => {
            if (like.user==user.id){
                result=true;
                return;
            }
        });
        return result;
    }

    hasAcces(isAuthenticated,user,news){
        if(isAuthenticated)
            return user.is_superuser||user.id==news.news_author;
        return false;
    }

    themSelect(news,them){
        if(them==0) return true
        if(them==news.them) return true
        return false
    }

    getThemName(news){
        var result="";
        this.props.thems.map(them =>{
            if (them.id==news.them)
                result=them.them_title
        })
        return result;
    }

    getLocalTime(pub_date){
        var date = new Date(pub_date);
        var options = {year: 'numeric', month: 'numeric', day: 'numeric', hour:'numeric', minute:'numeric', second:'numeric'};
        options.timeZone = 'Europe/Moscow';
        return date.toLocaleDateString('ru-RU', options);
    }
    
    render() {
        const{them} = this.state;

        const { isAuthenticated, user } = this.props.auth;
        const redHeart = (<nobr>❤</nobr>)
        const whiteHeart = (<nobr>🤍</nobr>)
        return (
            <Fragment>
                <h2>Новости</h2>
                <label>
                Выберите тему:  
            </label>
            <select name="them" value={them} onChange={this.onChange} className="form-select form-select-lg mb-3" aria-label=".form-select-lg example">
            <option key="0" value="0">Все</option>
                {this.props.thems.map(them => {
                    return <option key={them.id} value={them.id}>{them.them_title}</option>;
                })}
            </select>
                {isAuthenticated &&
                <div className="container">
                        <div  onClick={this.togglePopAdd}>
                        <button className="btn btn-info btn-sm">Добавить новость</button>
                        </div>
                        {this.state.seenAdd ? <Form toggle={this.togglePopAdd} /> : null}
                    </div>
                }
                <table className="table table-borderless">
                    <tbody>
                        {this.props.news.slice(0).reverse().map(news => (this.themSelect(news,them)&&
                            
                            <tr key={news.id}>
                                <td>
                                {this.hasAcces(isAuthenticated,user,news) &&
                                            <div style={{ display: "flex" }}>
                                                <div  onClick={this.togglePopEdit}>
                                                <button className="btn btn-warning btn-sm">Изменить</button>
                                                </div>
                                                <button onClick={this.props.deleteNews.bind(this, news.id)}
                                                className="btn btn-danger btn-sm"
                                                style={{ marginLeft: "auto"}}>
                                                    Удалить
                                                </button>
                                            </div>}
                                    <div className="card">
                                        <div className="card-body">
                                            <h6 className="card-subtitle mb-2 text-muted">{this.getThemName(news)}</h6>
                                            <h5 className="card-title">{news.news_title}</h5>
                                            <h6 className="card-subtitle mb-2 text-muted"><small>{this.getLocalTime(news.pub_date)} </small></h6>
                                            <p className="card-text" style={{whiteSpace: "pre-line"}}>{news.news_text}</p>
                                            <button onClick={isAuthenticated ? this.likeTongle.bind(this,this.isUserInLikes(user,news),user,news) : this.message.bind(this)}
                                                className="btn btn-light btn-sm"
                                                style={{ marginLeft: "auto"}}>
                                                 {isAuthenticated ? this.isUserInLikes(user,news) ? redHeart : whiteHeart : whiteHeart} {news.likes.length}
                                            </button>
                                            {this.state.seenEdit ? <EditForm toggle={this.togglePopEdit} id={news.id} them={news.them} news_text={news.news_text} news_title={news.news_title} news_author={news.news_author} pub_date={news.pub_date} /> : null}
                                        </div>
                                        
                                    </div>
                                    
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </Fragment>
        )
    }
}

const mapStatetoProps = state => ({
    thems: state.thems.thems,
    news: state.news.news,
    auth: state.auth,
})

export default connect(mapStatetoProps, { getNews, deleteNews, createMessage, addLike, deleteLike, getThems })(News)