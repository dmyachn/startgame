import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import {editNews} from "../../actions/news"
import {getThems} from "../../actions/thems"
import {getUsers} from "../../actions/users"
import { createMessage } from '../../actions/messages';


export class EditForm extends Component {
    state={
        id: this.props.id,
        them: this.props.them,
        news_title: this.props.news_title,
        news_text: this.props.news_text,
        news_author: this.props.news_author,
        pub_date: this.props.pub_date
    }

    static propTypes = {
        editNews: PropTypes.func.isRequired,
        getThems: PropTypes.func.isRequired,
        auth: PropTypes.object.isRequired,

    }

    onChange = e => this.setState({[e.target.name]: e.target.value});

    onSubmit = e => {
        e.preventDefault();
        this.state.pub_date=new Date().toISOString();
        if (!this.props.auth.user) {
          return
        }
        this.state.news_author=this.props.auth.user.id;
        const{id, them, news_text, news_title, news_author, pub_date} = this.state;
        const news = {id, them, news_text, news_title, news_author, pub_date};
        this.props.editNews(this.state.id,news);
        this.handleClick()
    };

    
    componentDidMount() {
        this.props.getThems();
    }


    handleClick = () => {
      this.props.toggle();
     };

    render() {
      const { isAuthenticated, user } = this.props.auth;

        const{them, news_text, news_title, news_author, pub_date} = this.state;
        return (
          <div className="popUpContainer">
          <span className="close" onClick={this.handleClick}>&times;    </span>
            <Fragment>
            
      <div className="card card-body mt-4 mb-4">
        <h2>Изменить новость</h2>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Заглоловок</label>
            <input
              className="form-control"
              type="text"
              name="news_title"
              onChange={this.onChange}
              value={news_title}
            />
          </div>
          <div className="form-group">
            <label>Текст новости</label>
            <textarea
              className="form-control"
              type="text"
              name="news_text"
              onChange={this.onChange}
              value={news_text}
            /> 
            </div>
            <label>
                Выберите тему:  
            </label>
            <select name="them" value={them} onChange={this.onChange} >
                {this.props.thems.map(them => {
                    return <option key={them.id} value={them.id}>{them.them_title}</option>;
                })}
            </select>
          <div className="form-group">
            <button type="submit" className="btn btn-primary" >
              Изменить
            </button>
          </div>
        </form>
      </div>
      </Fragment>
      </div>
    );
    }
}


const mapStatetoProps = state => ({
    thems: state.thems.thems,
    auth: state.auth
})

export default connect(mapStatetoProps, { editNews, getThems })(EditForm);
