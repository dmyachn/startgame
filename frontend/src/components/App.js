import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import Header from './layout/Header';
import Dashboard from './news/Dashboard';
import Them from './news/Them';
import Liked from './news/Liked';
import Login from './accounts/Login';
import Register from './accounts/Register';

import { HashRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

import AlertTemplate from 'react-alert-template-basic';
import { Provider as AlertProvider } from 'react-alert';

import Alerts from './layout/Alerts';

import { Provider } from 'react-redux';
import store from '../store'

import { loadUser } from '../actions/auth';


// Alert Options
const alertOptions = {
    timeout: 3000,
    position: 'top center',
  };

class App extends Component {
    componentDidMount() {
        store.dispatch(loadUser());
      }
    
    render() {
        return (
            <Provider store={store}>
            
        <AlertProvider template={AlertTemplate} {...alertOptions}>
            <Router>
                <Fragment>
                    <Header />
                    <Alerts />
                    <div className="container">
                        <Switch>
                            <Route exact path="/" component={Dashboard} />
                            <Route exact path="/register" component={Register} />
                            <Route exact path="/login" component={Login} />
                            <Route exact path="/liked" component={Liked} />
                            <Route exact path="/thems" component={Them} />
                        </Switch>
                    </div>
                </Fragment>
                </Router>
                </AlertProvider>
            </Provider>
        )
    }
}

ReactDOM.render(<App />, document.getElementById("app"));
