
import { GET_NEWS, DELETE_NEWS, ADD_NEWS, EDIT_NEWS } from './types';
import news from '../reducers/news';
import axios from 'axios';
import { createMessage, returnErrors } from './messages';

// get news
export const getNews = () => dispatch => {
    axios.get('api/v1/news/list/')
        .then(res => {
            dispatch({
                type: GET_NEWS,
                payload: res.data
            });
        })
        
    .catch((err) => dispatch(returnErrors(err.response.data, err.response.status)));
}

// delete news
export const deleteNews = (id) => dispatch => {
    axios.delete(`api/v1/news/detail/${id}`)
        .then(res => {
            dispatch(createMessage({deleteNews:"Новость удалена"}));
            dispatch({
                type: DELETE_NEWS,
                payload: id
            });
        }).catch(err => console.log(err));
}


// add news
export const addNews = news => dispatch => {
    axios.post('api/v1/news/create/', news)
        .then(res => {
            dispatch(createMessage({addNews:"Новость добавлена"}));
            dispatch({
                type: ADD_NEWS,
                payload: res.data
            });
        })
        
    .catch((err) => dispatch(returnErrors(err.response.data, err.response.status)));
}

// edit news
export const editNews = (id,news) => dispatch => {
    axios.put(`api/v1/news/detail/${id}/`, news)
        .then(res => {
            dispatch(createMessage({editNews:"Новость изменена"}));
            dispatch({
                type: EDIT_NEWS,
                payload: res.data
            });
        }).catch((err) => dispatch(returnErrors(err.response.data, err.response.status)));
}