export const GET_NEWS = "GET NEWS";
export const DELETE_NEWS = "DELETE NEWS";
export const ADD_NEWS = "ADD NEWS";

export const GET_THEMS = "GET THEMS";
export const DELETE_THEM = 'DELETE_THEM';
export const ADD_THEM = 'ADD_THEM';
export const EDIT_THEM = 'EDIT_THEM';

export const GET_USERS = "GET USERS";

export const USER_LOADING = 'USER_LOADING';
export const USER_LOADED = 'USER_LOADED';
export const AUTH_ERROR = 'AUTH_ERROR';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAIL = 'REGISTER_FAIL';
export const CLEAR_LEADS = 'CLEAR_LEADS';


export const GET_ERRORS = 'GET_ERRORS';
export const CREATE_MESSAGE = 'CREATE_MESSAGE';

export const ADD_LIKE = 'ADD_LIKE';
export const DELETE_LIKE = 'DELETE_LIKE';
export const EDIT_NEWS = 'EDIT_NEWS';
