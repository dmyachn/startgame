import { GET_USERS } from './types';
import users from '../reducers/users';
import axios from 'axios';

// get users
export const getUsers = () => dispatch => {
    axios.get('api/v1/user/list/')
        .then(res => {
            dispatch({
                type: GET_USERS,
                payload: res.data
            });
        }).catch(err => console.log(err));
}