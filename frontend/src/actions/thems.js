import { GET_THEMS, DELETE_THEM, ADD_THEM, EDIT_THEM} from './types';
import thems from '../reducers/thems';
import axios from 'axios';
import { createMessage, returnErrors } from './messages';


// get thems
export const getThems = () => dispatch => {
    axios.get('api/v1/them/list/')
        .then(res => {
            dispatch({
                type: GET_THEMS,
                payload: res.data
            });
        }).catch(err => console.log(err));
}

// delete them
export const deleteThem = (id) => dispatch => {
    axios.delete(`api/v1/them/detail/${id}`)
        .then(res => {
            dispatch(createMessage({deleteThem:"Тема удалена"}));
            dispatch({
                type: DELETE_THEM,
                payload: id
            });
        }).catch(err => console.log(err));
}


// add them
export const addThem = them => dispatch => {
    axios.post('api/v1/them/create/', them)
        .then(res => {
            dispatch(createMessage({addThem:"Тема добавлена"}));
            dispatch({
                type: ADD_THEM,
                payload: res.data
            });
        })
        
        .catch(err => console.log(err));
}

// edit them
export const editThem = (id,them) => dispatch => {
    axios.put(`api/v1/them/detail/${id}/`, them)
        .then(res => {
            dispatch(createMessage({editThem:"Тема изменена"}));
            dispatch({
                type: EDIT_THEM,
                payload: res.data
            });
        }).catch((err) => dispatch(returnErrors(err.response.data, err.response.status)));
}