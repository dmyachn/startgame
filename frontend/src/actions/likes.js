
import { ADD_LIKE, DELETE_LIKE } from './types';
import news from '../reducers/news';
import axios from 'axios';
import { createMessage, returnErrors } from './messages';


// delete like
export const deleteLike = (like, news) => dispatch => {
    axios.delete(`api/v1/like/detail/${like.id}`)
        .then(res => {
            dispatch(createMessage({deleteLike:"Новость удалена из избранного"}));
            dispatch({
                type: DELETE_LIKE,
                payload: like
            });
        }).catch(err => console.log(err));
}


// add like
export const addLike = like => dispatch => {
    axios.post('api/v1/like/create/', like)
        .then(res => {
            dispatch(createMessage({addLike:"Новость добавлена в избранное"}));
            dispatch({
                type: ADD_LIKE,
                payload: res.data
            });
        })
        
    .catch((err) => dispatch(returnErrors(err.response.data, err.response.status)));
}