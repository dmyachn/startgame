import { GET_NEWS, DELETE_NEWS, ADD_NEWS, DELETE_LIKE, ADD_LIKE, EDIT_NEWS } from "../actions/types.js"

const initialState = {
    news: []
}

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_NEWS:
            return {
                ...state,
                news: action.payload
            };
        case DELETE_NEWS:
            return {
                ...state,
                news: state.news.filter(news => news.id !== action.payload)
            };
        case ADD_NEWS:
            return {
                ...state,
                news: [...state.news, action.payload]
            };
        case DELETE_LIKE:{
            const index = state.news.findIndex(news => news.id ===action.payload.news); //finding index of the item
                const newArray = [...state.news]; //making a new array
                
                newArray[index].likes=newArray[index].likes.filter(like => like.id !== action.payload.id);//changing value in the new array
            return {
                ...state,
                news: newArray
            };
        }
        case ADD_LIKE:{
            const index = state.news.findIndex(news => news.id ===action.payload.news); //finding index of the item
                const newArray = [...state.news]; //making a new array
                newArray[index].likes.push(action.payload);//changing value in the new array
            return {
                ...state,
                news: newArray
            };
        }
        case EDIT_NEWS:
            {
                const index = state.news.findIndex(news => news.id === action.payload.id); //finding index of the item
                const newArray = [...state.news]; //making a new array
                newArray[index]=action.payload;//changing value in the new array
            return {
                ...state,
                news: newArray,
                // news: state.news.filter(news => news.id !== action.payload.id),
                // news: [...state.news, action.payload]
            }};
        default:
            return state;
    }
}