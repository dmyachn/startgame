import { GET_THEMS, DELETE_THEM, ADD_THEM, EDIT_THEM } from "../actions/types.js"

const initialState = {
    thems: []
}

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_THEMS:
            return {
                ...state,
                thems: action.payload
            };
            case DELETE_THEM:
            return {
                ...state,
                thems: state.thems.filter(them => them.id !== action.payload)
            };
        case ADD_THEM:
            return {
                ...state,
                thems: [...state.thems, action.payload]
            };
            case EDIT_THEM:
            {
                const index = state.thems.findIndex(them => them.id === action.payload.id); //finding index of the item
                const newArray = [...state.thems]; //making a new array
                newArray[index]=action.payload;//changing value in the new array
            return {
                ...state,
                thems: newArray,
            }};
        default:
            return state;
    }
}