import { combineReducers } from 'redux';
import news from './news';
import thems from './thems';
import users from './users';
import auth from './auth';
import errors from './errors';
import messages from './messages';
// import like from './like';

export default combineReducers({
    auth,
    news,
    thems,
    users,
    errors,
    messages,
    // like,
});