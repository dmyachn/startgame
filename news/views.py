from rest_framework.generics import CreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView
from news.serializers import UserSerializer, ThemSerializer, NewsSerializer, LikeSerializer
from .models import Them, News, User, Like


class ThemCreateView(CreateAPIView):
    serializer_class = ThemSerializer


class NewsCreateView(CreateAPIView):
    serializer_class = NewsSerializer


class UserCreateView(CreateAPIView):
    serializer_class = UserSerializer


class LikeCreateView(CreateAPIView):
    serializer_class = LikeSerializer


class ThemListView(ListAPIView):
    serializer_class = ThemSerializer
    queryset = Them.objects.all()


class NewsListView(ListAPIView):
    serializer_class = NewsSerializer
    queryset = News.objects.all()


class UserListView(ListAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()


class LikeListView(ListAPIView):
    serializer_class = LikeSerializer
    queryset = Like.objects.all()


class ThemDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = ThemSerializer
    queryset = Them.objects.all()


class NewsDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = NewsSerializer
    queryset = News.objects.all()


class UserDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()


class LikeDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = LikeSerializer
    queryset = Like.objects.all()
