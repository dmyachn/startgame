from django.urls import path
from news.views import *

app_name = 'news'
urlpatterns = [
    path('user/create/', UserCreateView.as_view()),
    path('user/list/', UserListView.as_view()),
    path('user/detail/<int:pk>/', UserDetailView.as_view()),

    path('them/create/', ThemCreateView.as_view()),
    path('them/list/', ThemListView.as_view()),
    path('them/detail/<int:pk>/', ThemDetailView.as_view()),

    path('news/create/', NewsCreateView.as_view()),
    path('news/list/', NewsListView.as_view()),
    path('news/detail/<int:pk>/', NewsDetailView.as_view()),

    path('like/create/', LikeCreateView.as_view()),
    path('like/list/', LikeListView.as_view()),
    path('like/detail/<int:pk>/', LikeDetailView.as_view()),
]