from django.db import models
from django.contrib.auth.models import User

class Them(models.Model):
    them_title = models.CharField('Название темы', max_length = 200)

class News(models.Model):
    them = models.ForeignKey(Them, on_delete=models.CASCADE)
    news_title = models.CharField('Название новости', max_length = 200)
    news_text = models.TextField('Текст статьи')
    news_author = models.ForeignKey(User, on_delete = models.DO_NOTHING)
    pub_date = models.DateTimeField('Дата публикации')
    
class Like(models.Model):
    news = models.ForeignKey(News, related_name='likes', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='likes', on_delete=models.CASCADE)