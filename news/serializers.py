from news.models import Them, News, User, Like
from rest_framework import serializers


class ThemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Them
        fields = '__all__'

class LikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Like
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

class NewsSerializer(serializers.ModelSerializer):
    likes=LikeSerializer(many=True, read_only=True)
    likes_count = serializers.SerializerMethodField()
    class Meta:
        model = News
        fields = ['id', 'them', 'news_title', 'news_text', 'news_author', 'pub_date', 'likes','likes_count']
    
    def get_likes_count(self, obj):
        return obj.likes.count()



